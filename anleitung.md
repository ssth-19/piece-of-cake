# Einleitung

*Piece of cake* ist ein Tabletop-Spiel à la Dungeons & Dragons mit einer Story, die den Spielern einen Einblick in Themen der IT-Security geben soll. Die Spieler schlüpfen dabei in die Rolle eines "Physical Pentesters" und treffen dabei auf verschiedene Themen wie Social Enginneering, Dumpster Diving, Password Security, Phishing usw. Die Spieler erzählen erzählen die Story selbst - mit etwas Hilfe vom Spielleiter. 

# So funktioniert's

Falls du schon einmal Dungeons und Dragons gespielt hast, weisst du eigentlich schon alles nötige. *Piece of Cake* funktioniert ganz ähnlich, aber benutzt nur einen ganz kleinen Teil der D&D Regeln. 

Das Prinzip funktioniert aber genau gleich. *Piece of Cake* ist eine Geschichte, die von den Spielern erzählt wird. Es gibt unzählige Möglichkeiten, wie sie ablaufen kann. Der Spielleiter ist dazu da, die Welt, in der sich die Spieler befinden, zu beschreiben und zu simulieren. Er seine Spieler durch die Geschichte und stellt sie vor Herausforderungen. Er kennt alle Geheimnisse der Welt und sollte für jede Situation gewappnet sein oder Improvisationstalent besitzen. Während Spieler normalerweise nur ihren eigenen Charakter spielen, erwecken Spielleitern jeden einzelnen (benötigten) NSC (Nicht-Spieler-Charakter) der Welt zum Leben. 

Der Ablauf im Spiel ist immer etwa wie folgt:

 1. Der Spielleiter beschreibt die aktuelle Umgebung, aber nur oberflächlich. Er erzählt den Spielern, was sie "auf den ersten Blick" sehen.
 2. Die Spieler entscheiden, was sie tun möchten. Z.b. etwas genauer anschauen
 3. Der Spielleiter sagt ihnen, was die Konsequenz ist. Z.b. gibt er eine genauere Beschreibung oder aber er Verlangt einen **Würfelwurf** um festzustellen, ob die geplante Aktion funktioniert. (Sie "Die Rolle der Würfel")

# Die Rolle der Würfel

Manchmal funktioniert im Leben nicht alles so wie man sich das vorstellt. In diesem Spiel wird Erfolg oder Misserfolg eines Plans durch die Würfel entschieden.
In *Piece of Cake* verwenden wir wie bei Dungeons & Dragons den 20-Seitigen Würfel für solche Entscheidungen, diese nennen sich "Ability-Checks". Davon gibt es mehrere Arten.

 * **Perception-Check** ("Wahrnehmungs-Check") (- Wird gemacht , wenn Spieler etwas genauer anschauen möchten. Je höher der Wurf, desto höher die Chance, dass die Spieler etwas entdecken, dass vorher nicht so offensichtlich war. Beispiel: "Ich möchte die Papiertonne durchsuchen" - bei einem erfolgreichen Check finen sie ein Papier mit wichtigen Hinweisen
 * **Deception-Check (CHARISMA) ** ("Anlügen") - Wird gemacht, wenn eine Person angelogen wird. Bei einem erfolgreichen Check glaubt die Person die aufgetischte Lüge, ansonsten wird sie misstrauisch
 * **Persuasion-Check (CHARISMA)** ("Überzeugen") - Ähnlich wie der Deception check- aber wird verwendet, wenn die Spieler nicht eine Lüge, sondern einen Fakt zur Überzeugung der Zielperson verwenden
 * **Intimidation-Check (CHARISMA)** ("Bedrohen") - Wird verwendet, wenn Spieler eine Drohung gegen eine Person aussprechen (z.b. "Wenn ich jetzt nicht auf's Klo darf, dann schreibe ich auf Yelp einen schlechten Review über ihre Bäckerei!")
 * **Strength-Check (ATHLETICS) k** ("Stärketest") - Wird verwendet, wenn eine Aktion Stärke erfordert, z.b. ein Regal beiseite schieben oder eine Tür eintreten.
 * **Nerd-Check** (NERD) ** - Wird für alles technische verwendet, z.b. Computer durchsuchen, Schlösser knacken
 * **Stealth Check ** - Wird gemacht, wenn die Spieler unbemerkt schleichen wollen. 

Und so funktioniert ein Check:
 1. Ein Spieler möchte etwas unternehmen, dass allenfalls schief gehen könnte. Beispielsweise "Ich will die Tür eintreten"
 2. Der Spielleiter legt (geheim) einen Schwierigkeitsgrad die Art des Checks (in diesem Fall "Strength-Check") und ein einen Schwierigkeitsgrad fest. Er kann dazu entweder einen Vorschlag aus der Beschreibung nehmen oder selber etwas bestimmen. Als Faustregel gilt
  * Einfach: Schwierigkeitsgrad `10`
  * Mittel: Schwierigkeitsgrad `15`
  * Schwer: Schwierigkeitsgrad `20`
 In diesem Beispiel nehmen wir `15` ( eine feste Tür, aber mit genug Kraft würde sie sich eintreten lassen. Bei einer schweren Eisentür würden wir vielleicht eine `20` festlegen.
3. Der Spieler würfelt nun mit seinem 20-er Würfel. Viele Checks gehören zu einer der Möglichen Charaktereigenschaften (oben in der Beschreibung in Klammern geschrieben). In diesem Fall "Athletics". Der Spieler rechnet in diesem Fall seinen Charakterwert zum Würfelergebnis hinzu (bzw. zieht davon ab, falls er einen negativen Wert hat). Im Beispiel würfelt der Spieler eine 12, aber er hat auf seinem Charakterblatt bei "Athletics" +3, d.h. sein Wert ist ingesamt 15, was gerade dem festgelegten Schwierigkeitsgrad entspricht, die Aktion ist also erfolgreich. Hätte er weniger gewürfelt, wäre die Tür nicht aufgegangen und die Spieler müssten einen anderen Weg suchen.

## Gruppen-Checks

Wenn mehrere Spieler eine Aktion gemeinsam unternehmen wollen, hat der Spielleiter mehrere Möglichkeiten, wie er das handhaben kann:

 * Zwei Spieler können sich *helfen* - in diesem Fall würfeln beide und das bessere Ergebnis zählt
 * Mehrere Spieler machen den Check und der (ungefähre) Durchschnitt gilt. Beispiel: Die Spieler möchten sich an der Bäckerstube vorbeischleichen. Der Spielleiter verlangt einen "Stealth-Check" und legt sich im Kopf Schwierigkeitsgrad 10 fest - alle spieler würfeln und der Spieleleiter nimmt sich einen "über-den Daumen durchschnitt" (eher über 10 oder eher unter 10.. das muss nicht genau sein. Wenn z.b. ein Spieler eine 20 würfelt darf er das auch als Besonders erfolgreich werten und dafür das schlechteste resultat ganz weglassen). Haben die Spieler als Gruppe tendentiell gut gewürfelt, schleichen sie sich erfolgreich vorbei, ansonsten werden sie bemerkt.


## Vorteil geben

Wenn die Spieler eine besonders gute Idee haben, kann der Spielleiter für einen Ability Check auch "Vorteil" geben. Mit Vorteil darf ein Spieler zwei Mal würfeln, und das bessere Ergebnis zählt.

## Vorbereitung für den Spielleiter

Als Spielleiter empfiehlt es sich, vorgängig die Regeln und die ganze Story schon mal durchzulesen, so dass du während dem Spiel nicht alles nachlesen musst. Du kannst dir dabei auch schon einige Gedanken machen, wie du die Nicht-Spieler-Charaktere spielen möchtest. Du kannst z.b. deine Stimme verstellen oder mit einem Akzent sprechen - oder auch ganz einfach in der dritten Person von ihnen reden ("der Bäckergehilfe fragt euch, was ihr hier tut!") - tu das, was dir am besten liegt.

## Bevor es losgeht

Die Spieler suchen sich einen der vorgefertigten Charaktere aus, und dürfen diesen noch etwas anpassen, falls sie das möchten. Sie können dem Charakter z.b. einen eigenen Namen geben und seine Beschreibung verändern.

## Das Spiel beginnt

Lies den Spielern nun die Einleitung vor ( oder verwende deine eigenen Worte, wie du lieber magst).

Danach ist es an den Spielern, einen Plan zu Fassen, wie sie das Rezept zurückbekommen. Lasse ihnen dabei so viele Freiheiten wie nur möglich.

