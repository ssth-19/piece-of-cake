# List of characters
## Family Business
 * [Chef Senior](#chef-senior)
 * [Chef Junior](#chef-junior)
 * [Lehrling](#Lehrling) 
 * [Bäckerin](#bäckerin)
 * [Fahrer](#fahrer)
 * [Verkäuferin](#verkäuferin)

## Rivale business
 * [Chef](#chef-rivale)
 * [Sekretärin](#sekretärin-rivale)
 * [Verkäufer](#verkäufer-rivale)
 * [2x Baker](#bäcker-rivale)
 * [Lehrling](#Lehrling-rivale)

# Spielercharakter
## Chef Junior
* Name: Max
* Geschlecht: m
* Alter: 36
* Aussehen:
  * Haarfarbe: braun
  * spezial: Grübchen
* Eigenschaften:
  * grosszügig
  * hingebungsvoll
  * geduldig
  * familienorientiert
* Spezial Eigenschaft: 
  * Charisma
* Stats:
   
   | Eigenschaft  | Pts|
   | ------------ | -- |
   | Intelligenz  |  3 |
   | Charisma     |  5 |
   | Athletik     | -1 |
   | Nerd         | -1 |
   
* Beschreibung:  
Max wuchs in einer liebevollen Umgebung mit seinen zwei Geschwistern auf. Schon früh half er seinen Eltern in der eigenen Bäckerei aus. Die Arbeit, die Max Eltern für ihn und seine Familie machten, wusste er stets zu schätzen. Für ihn war schon sehr früh klar, dass er das Familienunternehmen übernehmen und weiterführen wollte. Da seine zwei Schwestern kein Interesse an der Bäckerei zeigten, war das kein Problem. Vor ein paar Jahren hat er die Bäckerei von seinem Vater übernommen. Er ist mittlerweile verheiratet und hat zwei Kinder. 


## Lehrling
* Name: Elliot
* Geschlecht: m
* Alter: 18
* Aussehen:
  * Haarfarbe: Braun
  * Spezial: Brille
* Eigenschaften:
  * witzig
  * technisch versiert
  * introvertiert
  * intelligent
* Spezial Eigenschaft: 
  * Technik
* Stats:  

   | Eigenschaft  | Pts|
   | ------------ | -- |
   | Intelligenz  |  3 |
   | Charisma     | -1 |
   | Athletik     | -1 |
   | Nerd         |  5 |
   
* Beschreibung:  
Elliot ist leidenschaftlicher Computerliebhaber. Wenn es nach ihm ginge, hätte er eine Lehre als Informatiker gemacht. Seine Eltern haben ihn aber dazu überredet sein zweites Hobby, nämlich das Backen, zum Beruf zu machen. In seiner Freizeit findet man ihn oft am Computer. Wenn er nicht gerade am Fortnite spielen ist, schaut er seine Lieblingsserie Mr. Robot. Obwohl er die Technik nicht zum Beruf gemacht hat, liebt er die Arbeit in der Bäckerei sehr. Er fühlt sich schon wie zuhause und seine Teamkollegen sind wie eine zweite Familie für ihn. Er traut sich auch schon ab und zu einen Witz zu machen.

## Bäckerin
* Name: Kelly
* Geschlecht: f
* Alter: 28
* Aussehen:
  * Haarfarbe: Rot
  * speziall: Sommersprossen
* Eigenschaften:
  * lässig
  * abenteuerlich
  * mutig
* spezial Eigenschaft: 
  * Geschicklichkeit
* Stats:  

   | Eigenschaft  | Pts|
   | ------------ | -- |
   | Intelligenz  |  1 |
   | Charisma     |  2 |
   | Athletik     |  4 |
   | Nerd         | -1 |
   
* Beschreibung:  
Seit ihrer Kindheit sucht Kelly immer wieder nach einem Adrenalinrausch: entweder auf Achterbahnen, in der Wildnis oder beim Bungee-Jumping. Sie würde fast alles machen. Im Gegensatz zu ihren abenteuerlichen Freizeitbeschäftigungen liebt sie die Ruhe in der Backstube, welche ihr genug Zeit gibt, um sich für das nächste Abenteuer zu erholen. Um sich fit zu halten, geht sie jeden Tag mit ihrem drei Jahre alten Hund joggen. Auf den ersten Blick sieht sie nicht sehr stark aus, man darf sie wegen ihrer Geschicklichkeit aber auf keinen Fall unterschätzen.

## Fahrer
* Name: Tom
* Geschlecht: m
* Alter: 47
* Aussehen:
  * Haarfarbe: Braun, hat aber Glatze
  * spezial: Glatze
* Eigenschaften:
  * Leseratte
  * friedlich
  * zuverlässig
* spezial Eigenschaft: 
  * Wissen
* Stats:  

   | Eigenschaft  | Pts|
   | ------------ | -- |
   | Intelligenz  |  3 |
   | Charisma     | -2 |
   | Athletik     |  1 |
   | Nerd         |  3 |
   
* Beschreibung:  
Tom ist das neuste Mitglied der Bäckerei und ist gerade erst mit seiner Freundin und seinen vier Katzen in die Stadt gezogen. Vorher hatte er für unzählige Unternehmen die unterschiedlichsten Güter ausgeliefert. Bis jetzt konnte er nie lange beim gleichen Job bleiben und hofft nun, dass er seine Traumstelle gefunden hat. Zwischen zwei Lieferungen sieht man ihn oft in der Fahrerkabine am Bücher lesen während er ein Stück des berühmten Kuchens geniesst. Seine Arbeitskollegen witzeln, dass er alles weiss. Sie haben ihn sogar mal gesehen, wie er ein Buch über Schlösserknacken gelesen hat. Als ob eine so friedliche Person jemals ein Schloss knacken müsste....

## Verkäuferin
* Name: Margrith
* Geschlecht: f
* Alter: 56
* Aussehen:
  * Haarfarbe: Braun
* Eigenschaften:
  * mütterlich
  * vertrauenswürdig
  * zärtlich
* spezial Eigenschaft: 
  * Überzeugung
* Stats:  

   | Eigenschaft  | Pts|
   | ------------ | -- |
   | Intelligenz  |  3 |
   | Charisma     |  6 |
   | Athletik     | -2 |
   | Nerd         | -1 |

* Beschreibung:    
Margrith ist eine liebevolle Mutter von vier erwachsenen Kindern, welche leider alle sehr weit weg wohnen. Sie arbeitet schon ihr Leben lang als Verkäuferin und kann sich auch gar nicht vorstellen, etwas anderes zu tun. Sie liebt es, wenn sie das Lachen auf dem Gesicht ihrer Kunden sieht, sobald diese eines der feinen Gebäckstücke probieren. In ihrer Freizeit backt sie für ihr Leben gern und bringt manchmal sogar ihre hausgemachten Cookies mit um diese ihren Arbeitskollegen und Kunden zu geben. Leider ist ihr Knie nicht mehr ganz das jüngste und macht nicht mehr alles mit, was sie gerne möchte.


# Spielleiter Charakter
## Chef Senior
* Name: Markus
* Geschlecht: m
* Alter: 65
* Aussehen:
  * Haarfarbe: Grau
  * spezial: Falten, lacht viel
* Eigenschaften:
  * liebevoller Vater
  * familienorientiert
  * Arbeitstier
* spezial Eigenschaft: 
  * Charisma
* Beschreibung:  
Markus hat bis vor ein paar Jahren die Familienbäckerei geführt, bis er das Zepter seinem Sohn Max übergeben hat. Er liebt seine Familie und seine Bäckerei über alles und würde alles für sie tun. Nebst seiner Familie hat er nicht viele Freunde und auch sehr wenige Hobbies. Er versteht sich aber dank seinem charismatischen Charakter mit fast jedem. Er überlegt sich, einen Hund zu besorgen.

## Chef Rivale
* Name: Jenny
* Geschlecht: f
* Alter: 43
* Aussehen:
  * Haarfarbe: Rot
  * spezial: schaut grimm aus
* Eigenschaften:
  * Arbeitstier
  * selbstbewusst
  * kühn
* spezial Eigenschaft:
  * Einschüchterung
* Beschreibung:  
Jenny hatte eine harte Kindheit und hat schon früh gelernt, dass sie nur auf sich selbst zählen kann. Ihr Wirtschaftsstudium hat sie an einem der besten Universitäten der Welt abgeschlossen. Mit 27 Jahren hat sie bereits ihr eigenes Unternehmen eröffnet, welches mittlerweile sehr erfolgreich ist. Sie würde alles machen, damit ihr Unternehmen es an die Spitze schafft. In ihrer Freizeit fischt sie sehr gerne, denn das war für sie früher der einzige Weg, Zeit mit ihrem bereits verstorbenen Vater zu verbringen.

## Sekretär Rivale
* Name: John
* Geschlecht: m
* Alter: 25
* Aussehen:
  * Haarfarbe: Blond
  * spezial: trägt farbenfrohe Kleider
* Eigenschaften:
  * pflanzenliebhaber
  * kontaktfreudig
  * hilfsbereit
* spezial Eigenschaft:
  * Gerüchte
* Beschreibung:  
John stammt von einer grossen Familie und hat sehr viele Freunde. Er liebt es über die neusten Geschehnisse zu plaudern und kennt alle Gerüchte. Er würde seine Karriere gerne weiterentwickeln, aber aktuell steckt er als Sekretärin in einer langweiligen Bäckerei fest. Sein Traum ist es, dass er von dem öden Alltag fliehen kann und mit seinen Freunden die Welt zu bereisen.

## Verkäufer Rivale
* Name: Marc
* Geschlecht: m
* Alter: 59
* Aussehen:
  * Haarfarbe: Schwarz
* Eigenschaften:
  * gemein
  * persistent
  * ungeduldig
* spezial Eigenschaft: 
  * Leute fühlen sich schlecht um ihn herum
* Beschreibung:  
Marc hat alles gesehen, was die Arbeitswelt zu bieten hat. Er hat die Nase voll von seinem Job und möchte nur noch in den Ruhestand gehen. Er hasst es, wenn sich die Kunden nicht entscheiden können und Stunden brauchen, bis sie endlich etwas bestellen. Als hätte er Zeit für so ein Theater. Er weiss immer sofort, was er will. Und ganz bestimmt will er diesen Job nicht. Nur noch ein paar Jahre, bis er den ganzen Tag am See fischen gehen kann und diese Knochenarbeit hinter sich lassen kann.

## Bäcker Rivale
* Name: Billy
* Geschlecht: m
* Alter: 33
* Aussehen:
  * Haarfarbe: Schwarz
  * spezial: sieht böse aus & ist gross
* Eigenschaften:
  * freundlich
  * schüchtern
  * stark
* spezial Eigenschaft:
  * Einschüchterung (wegen Aussehen)
* Beschreibung:  
Schon seit er jung ist, möchte Billy ein Bäcker werden sein. Seine Mitschüler haben sich früher über ihn lustig gemacht, weil er so gross ist. Deshalb war er oft alleine unterwegs und ist auch bis heute noch sehr schüchtern. Billy sieht auf den ersten Blick sehr unfreundlich aus. Sobald man aber anfängt mit ihm zu sprechen, merkt man schnell, dass er nur ein schüchterner und etwas ängstlicher Mann ist. Er lebt alleine mit seinen drei Schildkröten.

## Bäckerin Rivale
* Name: Anna
* Geschlecht: w
* Alter: 30
* Aussehen:
  * Haarfarbe: Schwarz mit grünen Streifen
  * spezial: viele Tattoos
* Eigenschaften:
  * sportlich
  * humorvoll
  * sarkastisch
* spezial Tratis
  * Geschichten erzählen
* Beschreibung:  
Anna arbeitet erst seit zwei Jahren in der Bäckerei. Sie liebt das Backen aber ihr gefällt nicht, wie ihr Arbeitgeber seine Mitarbeiter behandelt. Ihr Arbeitskollege Billy ist einer ihrer besten Freunde. Anna startet und endet fast jeden Tag mit einem Training, denn sie liebt Sport. Sie ist von der Kunst und von Tätowierungen fasziniert. Sie liebt es, dass sie mit ihren Tätowierungen eine Geschichte erzählen kann. Sie überlegt sich sogar, ob sie ihren aktuellen Job aufgeben soll und ihr eigenes Tätowier-Studio eröffnen soll.

## Lehrling Rivale
* Name: Alfred
* Geschlecht: m
* Alter: 16
* Aussehen:
  * Haarfarbe: Braun
  * speziall: dünn
* Eigenschaften:
  * listig
  * extrovertiert
  * singt gerne
* spezial Eigenschaft:
  * Keine
* Beschreibung:  
Alfred war ein sehr guter und normaler Schüler. Er spielt gerne Fussball und singt in einem lokalen Chor. Er wusste zuerst nicht, was er für einen Beruf erlernen sollte. Als sein Onkel im vorgeschlagen hat, Bäcker zu werden, dachte er, dass das eine gute Idee ist. Jetzt ist er sich jedoch nicht mehr ganz so sicher, weil er nur den Abwasch und die Putzarbeit erledigen darf.
