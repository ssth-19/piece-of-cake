# Infos Sammeln vor dem Besuch zur Bäckerei

Falls die Charaktere die Webseite der Bäckerei anschauen, finden Sie:

 * Auf der Homepage steht gross die die Ankündigung einer **brandneuen Kuchenkreation**, welche bereits nächste Woche erhältlich sein soll
 * Oben Rechts befindet sich ein **Loginformular für den Intranetbereich**
 * Auf der Kontaktseite steht die **Adresse** der Bäckerei mit dem Hinweis, dass sich die Anlieferung hinter dem Haus bgefindet, sowie die **Telefonnummern** vom Laden und die Telefonnummer des Sekretariats

Falls die Charaktere bei der Bäckerei anrufen, oder ein Phishing-Email senden möchten, gelingt ihnen dies mit einem Charisma Check 13 (Intimidation oder Persuasion). Sie können sich damit beispielsweise Zugriff auf das Intranet verlangen, oder den Eintritt in die Bäckerei erleichtern, oder erfahren, dass noch niemand der angestellten das neue Kuchenrezept kennt. Der Chef sei da sehr geheimnistuerisch.

Die Charakter können versuchen, dass Intranet auf technischem Weg zu knacken, beispielweise durch eine Brute-Force Attacke, eine SQL-Injection  ( Nerd Check 12). 
Dort finden Sie einen Bauplan der Bäckerei, welche offenbar bald umbauen möchte. Verteile den Spielern in diesem Fall das Handout "Plan der Bäckerei Honeypot"


# EG

## ***Parkplatz / Eingang***

Nach einer kurzen Autofahrt erreicht Ihr das Gelände der Bäckerei **Honey pot**. Ihr stellt euren Lieferwagen etwas abseits an eine Strasse und schaut euch um.

Die Bäckerei ist ein Eckhaus an der Hauptstrasse und einer Seitenstrasse. Vor dem Haus befinden sich drei **Parkplätze**, die an die Seitenstrasse grenzen. An der Ecke zur Seitenstrasse seht ihr ein Schild mit der Aufschrift **"Lieferanteneingang hinter dem Haus"**. Vom Parkplatz aus seht ihr durch die grossen Glasscheiben in den Verkaufsraum der Bäckerei. Ein Duft von frischem Brot mit einem Hauch Süsse liegt in der Luft.

Der Eingang der Bäckerei liegt gegenüber der Parkplätze. Eine kleine **Steintreppe** mit drei Stufen führt hinauf. Neben der Treppe steht ein **Schild** mit den aktuellen Tageshits und der Überschrift: **The cake is NOT a lie - something new coming soon**. Der Eingang ist eine automatische Schiebetüre. Über dem Eingang sowie an den Glasscheiben ist die Aufschrift mit dem Namen der Bäckerei **"Honey Pot"** zu lesen. An den Glasscheiben rund um den Verkaufsraum kleben Aufkleber mit einer lächelnden Biene. 

* 
* 
* 

## ***Verkaufsraum***

Als ihr den **Verkaufsraum** betretet, wird der Duft stärker. Vor euch seht ihr eine **Thekenzeile** mit vielen Leckereien hinter einer Glasvitrine. In der Auslage auf der linken Seite befinden sich frische Gipfeli und Brötchen, daneben frische belegte Brote gefolgt von süssen Backwaren, Muffins, Cookies und Küchlein. Links Aussen bei der Theke steht die **Kasse** mit einem Touch-Screen. Hinter der Theke an der Wand füllt gerade die Verkäuferin frische Brote ins Brotauslagegestell. Neben dem Brotgestell steht eine Kaffeemaschine, die leise vor sich hin dampft.

An der Wand rechts von euch ist eine Tür ersichtlich. An ihr hängt ein Schild mit der Aufschrift **Toilette nur für Kunden**.

Links von euch hängt eine kleinen Holz-Garderobe mit 4 Haken an der Wand. Zwischen der Garderobe und der Theke führt ein schmaler Gang zu zwei runden Tischen mit je zwei Stühlen und einer Sitzbank. Auf jedem Tisch steht eine laminierte Speise- und Getränkekarte. 

* Hinten an der Wand hängt ein Schild mit der Aufschrift **Free WiFi Passwort = 123456**. 
* Wenn man genauer hinsieht ist das WiFi Passwort bei den Tischen hinten ersichtlich (Nerd Check 10) und wenn man sich verbindet, können sie mit Nerd Check 12 auf interne Daten zugreifen.  Sie erhalten Zugriff auf Baupläne, da nächstes Jahr eine Vergrösserung der Backstube geplant ist.
* Evt. Unlocked Handy des Verkäufers?

## ***Gang EG nach Verkaufsraum***

Durch die Tür beim Verkaufsraum gelangt ihr in einen dunklen Gang. Flackernde Neonröhren erhellen ihn. Zu eurer Linken am Ende des Ganges ist eine Wendeltreppe zu erkennen. Rechts von euch verliert sich der Gang in einer Kurve.

* 
* 
* 

## ***Aufenthaltsraum***

Der Aufenthaltsraum ist einfach gehalten. Es ist warm im Raum. Ein Tisch mit Stühlen steht rechts in der Ecke. Auf einem Teller liegt ein angebissenes Sandwich, das von einer Fliege umkreist wird. Geradeaus erkennt ihr eine Tür mit der Aufschrift **Putzschrank**. Es ist ein summendes Geräusch daraus zu hören.

Rechts von euch steht eine Küchenzeile mit einer Mikrowelle und einer Kaffeemaschine. Oberhalb hat es einen offenen Küchenschrank mit Tassen und Gläsern. Hinter der Küchenzeile steht ein Getränkeautomat.

* Serverraum mit WLAN Router im Putzschrank finden (Intelligenzcheck 15) und wenn man sich verbindet, können sie mit Nerd Check 12 auf interne Daten zugreifen. Sie erhalten Zugriff auf Baupläne, da nächstes Jahr eine Vergrösserung der Backstube geplant ist.
* 
* 

## ***WC***

Die Toilette ist ein schmaler rechteckiger Raum, mit einem milchigen Glasfenster, welches gekippt ist. Es ist schlicht gehalten mit einer Toilette, einem Waschbecken und einem Spiegel. Neben dem Waschbecken steht ein Abfallkorb.

* 
* 
* 

## ***Lieferanteneingang***

Die Strasse biegt nach links um das Haus ab. Nach 7 Metern seht ihr die **verschlossene Lieferantentüre**. Oberhalb der Türe flackert ein Scheinwerferlicht. Die Türe ist mit einem **elektronischen Türschloss mit Zahlencode** verschlossen. Links von der Türe stehen zwei Stühle und ein Tisch. Auf dem Tisch steht ein Aschenbecher mit einer halb fertigen noch brennenden Zigarette. Rechts von der Türe stehen **offene Kartonschachteln und zwei Abfalltonnen**. Eine der Tonnen ist mit Betriebskehricht beschriftet. Die andere Tonne ist mit Papier beschriftet. Nach den Tonnen steht ein Fenster offen. Es brennt Licht und ihr nehmt einen leichten Duft nach frischem Kuchen wahr.

* Mit einem Athletic Check 12 können Sie in die Müllcontainer Klettern. Dabei finden Sie eine Abrechnung für einen neuen Safe (Dumpster Diving)
* Falls draussen der Lehrling am rauchen ist kann man ihm mit einem Charisma Check 15 ETWAS entlocken. 
* 

## ***Gang EG nach Lieferanteneingang***

Durch den Lieferanteneingang betretet ihr einen Gang. Zu eurer Linken gibt es zwei Türen und rechts von euch eine Türe. Die Türe direkt links von euch ist ein Spalt weit offen.

* 
* 
* 

## ***Lager***

Im Lager ist es dunkel. Durch das vom Gang einfallende Licht seht ihr Regale an der Wand. Der ganze Raum ist vollgestellt mit Regalen bis zur Decke. Im Raum duftet es nach frischen Kaffeebohnen. Rechts von der Türe hat es ein Lichtschalter. Die Regale rechts von euch sind gefüllt mit Mehlpackungen, Zucker und Salz. Hinten an der Wand stehen sehr viele Packungen mit unterschiedlichen Kaffeesorten. Die Kartonschachteln in den Regalen links von euch sind nicht beschriftet.

* 
* 
* 

## ***Kühlraum***

Das Licht vom Kühlraum schaltet sich automatisch an. Links und rechts stehen hohe Regale mit Milchpackungen, Hefe, Rahmgläsern und verschlossenen Kartonschachteln. An der hinteren Wand entdeckt ihr eine massive Türe mit der Beschriftung **Tiefkühlraum**.

* 
* 
* 

## ***Backstube***

Ihr öffnet die Türe zur Backstube und es strömt euch sofort ein leckerer Duft nach frischem Kuchen entgegen. Als ihr die Backstube betretet, seht ihr vor euch in der Mitte des Raumes einen **grossen Arbeitstisch**. An der Wand zu eurer Linken stehen zwei **kleinere Tische**. Vor euch stehen **Rollwagen** mit gebackenen Brötchen. An der hinteren Wand befindet sich eine **Industriewaschmaschine**, die schon leicht mitgenommen aussieht. Zwei **kleine Fenster** dahinter sorgen für etwas Licht im Raum. Rechts von euch gibt es **zwei grosse Backofen**. An der Wand zu eurer Rechten steht ein **Regal** gefüllt mit Geschirr und Arbeitsmaschinen. Nach dem Regal steht ein Fenster offen mit Blick auf ein geparktes Auto.

* 
* 
* 

# OG

## ***Gang***

Die Wendeltreppe hinauf führt zu einem grossen breiten Gang. Es ist sehr hell. Es sind **vier Türen** zu sehen. Wovon die erste links aus Glas ist. Man kann hindurchsehen und weitere Tische und Stühle sind im Raum dahinter zu erkennen. 

Gleich links neben der Wendeltreppe und der Glastüre ist eine Sitzecke eingerichtet, mit zwei braunen Einzelsessel und einem ovalen Salontisch aus Glas. Ein paar Magazine mit der Aufschrift "Backen Heute" sind darauf verteilt. 

An den Wänden im Gang hängen auf jeder Seite 4 Bilder die das Gebäude in verschiedenen Zeiten und Epochen wiedergeben. Einige Bilder sind schwarzweiss, andere gelblich verfärbt und andere wiederum in Farbe. 

Die Tür neben dem Sitzungszimmer ist mit **Sekretariat** angeschrieben.

## ***Sitzungszimmer***

Durch die Glastüre im Gang gelangt ihr ins Sitzungszimmer. Ein grosser massiver runder Eichen-Tisch steht im Zentrum des Raumes ummrandet von schwarzen Bürostühlen. Eine Wand aus Einbauschränken in der linken Seite des Raumes schliesst das Situngszimmer. 

* Mit einem Nerd Check 12 findet man ein LAN Kabel auf dem Sitzungstisch, damit gelangt man ins interne Netz und auf interne Daten zugreifen. Sie erhalten Zugriff auf Baupläne, da nächstes Jahr eine Vergrösserung der Backstube geplant ist.

## ***Sekretariat***

Im Sekretariat angekommen duftet es herrlich nach einem frischen Blumenstraus. In der rechten Ecke steht ein Schreibtisch mit einem Blumenstraus, einem Computer und einem Rollkorpus. Links im Raum steht ein zwei-türiger abschliessbarer Schrank neben einem Multifunktionsprinter. Die Drucker Anzeige blinkt gerade rot auf. 

Neben der Türe ist ein 40 x 50 cm hoher silberner Schlüsselschrank montiert. 

In der rechten Ecke des Raumes steht noch ein voller Aktenvernichter.  

* Post-it mit Passwort? 
* Gleiches Passwort PC und Handy?
* Dokument Aktenvernichter

## ***Büro Chef***

Als ihr das Büro betretet, seht ihr direkt vor euch in der linken Ecke des Zimmers **ein Sofa mit Tisch und zwei Sesseln**. Rechts davon steht ein **grosses Pult mit Flachbildschirm und Tastatur sowie ein Bürostuhl**. Auf dem Tisch liegen **Back-Bücher**. Auf der rechten Seite ist über die ganze Wandbreite ein **überfülltes Bücherregal** aufgestellt. Direkt rechts neben der Türe steht ein Aquarium mit vielen kleinen Anemonenfischen. Grobkörniger Quarzsand, schwarze Vulkansteine und Schwertfische aus Plastik zieren den Boden des Aquariums.  Eine Korkrinde und Pflanzen dienen als Versteck für die Fische. Auf der Ablage neben dem Aquarium steht eine zylinderförmige Dose Fischfutter. 

* Intelligenz Check 12 zeigt PC-Passwort  unter Tastatur
* Passwortliste mit mehrfach verwendeten PWs für verschiedene Accounts
* Social Media Posts?
* PC-Chef Updates? 

## ***Terrasse***

Durch die grosse Glas-Doppeltüre betretet ihr die Terrasse. Vor euch am Rande der Terrasse steht ein Grill. Links von euch steht ein Tischtennis Tisch. Rechts steht ein Tisch mit zwei Sitzbänken. Zwei grosse Palmen zieren je eine Ecke der Terrasse.
