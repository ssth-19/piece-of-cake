# List of characters
## Family business
 * [Boss Senior](#boss-senior)
 * [Boss Junior](#boss-junior)
 * [Apprentice](#apprentice) 
 * [Baker](#baker)
 * [Delivery Guy](#delivery-guy)
 * [Saleswoman](#saleswoman)

## Rivalry business
 * [Boss](#boss-rivalry)
 * [Secretary](#secretary-rivalry)
 * [Salesman](#salesman-rivalry)
 * [2x Baker](#baker-rivalry)
 * [Apprentice](#apprentice-rivalry)

# Characters played by Players
## Boss Junior
* Name: Max
* Gender: m
* Age: 36
* Appearance:
  * haircolor: brown
  * special: has dimples
* Traits:
  * generous
  * devoted
  * patient
* Special Traits: 
  * Charisma
* Stats:
   
   | Trait        | Pts|
   | ------------ | -- |
   | Intelligence |  3 |
   | Charisma     |  5 |
   | Athletics    | -1 |
   | Nerd         | -1 |
   
* Description:  
Max was raised together with his two siblings with much love from his parents. He learnt early on to help out in the family business and never took anything for granted. He appreciated all the work his parents did for him and wished to make them proud. It was crystal clear for him that he wanted to take over the business. Since his two sisters weren't interested and went to study law and mathematics this was never questioned. He has a wife and two lovely children. At the age of 34 he took over from his father and wants to make his father and ancestors proud in continuing baking the best cakes in the world. 


## Apprentice
* Name: Elliot
* Gender: m
* Age: 18
* Appearance:
  * haircolor: brown
  * special: has glasses
* Traits:
  * tech savvy
  * introverted
  * witty
* Special Traits: 
  * Nerd
* Stats:  

   | Trait        | Pts|
   | ------------ | -- |
   | Intelligence |  3 |
   | Charisma     | -1 |
   | Athletics    | -1 |
   | Nerd         |  5 |
   
* Description:  
Elliot is passionate about computers. If it were up to him he would have done an apprenticeship in IT but his parents persuaded him to become a baker. Most of his freetime is spend on the computer playing Fortnite or watching his favourite series Mr.Robot. He think it is faith that he has the same name as the protagonist. Although not making his hobby into a profession he still loves working at the bakery. The work atmosphere is great and it feels like a second family to him. He already opened up a bit and even cracks a joke from time to time.

## Baker
* Name: Kelly
* Gender: f
* Age: 28
* Appearance:
  * haircolor: red
  * special: has freckles
* Traits:
  * easy going
  * adventurous
  * brave
* Special Traits: 
  * Dexterity
* Stats:  

   | Trait        | Pts|
   | ------------ | -- |
   | Intelligence |  1 |
   | Charisma     |  2 |
   | Athletics    |  4 |
   | Nerd         | -1 |
   
* Description:  
Since childhood Kelly loves everything that gives her an adrenaline rush. If it's roller coaster, hiking in the wilderness or bungee jumping. She's up for almost everything. In contrary to her adventurous free time she loves the quiet and the sometimes monotonous work life. This gives her enough time to recover for the next adventure. To keep her in shape she regularly goes jogging with her 3 year old dog. At first sight you wouldn't give her being so sportive but she's swift.

## Delivery Guy
* Name: Tom
* Gender: m
* Age: 47
* Appearance:
  * haircolor: brown but bald
  * special: bald
* Traits:
  * avid reader
  * peaceful
  * reliable
* Special Traits: 
  * Knowledge
* Stats:  

   | Trait        | Pts|
   | ------------ | -- |
   | Intelligence |  3 |
   | Charisma     | -2 |
   | Athletics    |  1 |
   | Nerd         |  3 |
   
* Description:  
Tom is the newest member of the family bakery and just moved to the city together with his girlfriend and four cats. Before moving here he worked at different companies delivering a wide variety of goods but he never could stay at one job for long. His hopes are high that he has found the perfect spot for him. Between deliveries you can sometimes spot him reading one of his books in the car while snacking on a piece of that delicious cake. His coworkers joke that he knows everything. They even spotted him reading a book about lock picking a few weeks ago. As if such a peaceful person ever came in need to pick a lock...

## Saleswoman
* Name: Margrith
* Gender: f
* Age: 56
* Appearance:
  * haircolor: brown
* Traits:
  * motherly
  * trusts easily
  * affectionate
* Special Traits: 
  * Persuasion
* Stats:  

   | Trait        | Pts|
   | ------------ | -- |
   | Intelligence |  3 |
   | Charisma     |  6 |
   | Athletics    | -2 |
   | Nerd         | -1 |

* Description:    
Margrith is a loving mother of 4 children who already moved out of her home and live far away. She has been working as a saleswoman for a long time and can't imagine doing anything else. She loves to see the smile on the customers face when they taste their baked goods especially their infamous cake. She likes to bake and sometimes even brings her own cookies to the bakery to give out to customer or her work colleagues. She has some problems with her knee which probably will get worse with old age.


# Characters played by DM
## Boss Senior
* Name: Markus
* Gender: m
* Age: 65
* Appearance:
  * haircolor: greyish
  * special: Wrinkles, smiles a lot
* Traits:
  * loving father
  * family oriented person
  * loves to work
* Special Traits: 
  * Charisma
* Description:  
Was running the family business till a few years ago when his son took over. He loves his family and their business and would do anything for them. Doesn't have a lot of hobbies/friends but gets along with everyone because of his easygoing character. Is thinking about getting a dog.


## Boss Rivalry
* Name: Jenny
* Gender: f
* Age: 43
* Appearance:
  * haircolor: red
  * special: grim looking
* Traits:
  * workaholic
  * confident
  * bold
* Special Traits:
  * Intimidation
* Description:  
Had a hard childhood and learnt early on to rely only on herself. Went to a top university to study economics. She opened her own business at the age of 27 and it is a real success. She would do anything to get her business on top of the market. She likes to fish because that was the only time she bonded with her father who sadly already passed away. 


## Secretary Rivalry
* Name: John
* Gender: m
* Age: 25
* Appearance:
  * haircolor: blond
  * special: wears a lot of colorful clothes
* Traits:
  * loves plants
  * outgoing
  * ready to help
* Special Traits:
  * Gossip
* Description:  
Comes from a big family and has a lot of friends. He likes to gossip and knows everything that's going on around him. Went to college but never made it to university. Would like to further his career but is currently stuck as a secretary at a boring bakery. Wish he could get away and travel the world with friends. 


## Salesman Rivalry
* Name: Marc
* Gender: m
* Age: 59
* Appearance:
  * haircolor: black
* Traits:
  * mean
  * persistent
  * impatient
* Special Traits: 
  * good at making People feel bad
* Description:  
Marc has seen everything that work life has to offer. He is fed up with his work and just want to get retired.  He hates that people sometimes don't know what they want and take hours selecting the products they want. As if he had time. He always knew exactly what he wanted, and it's for sure that he didn't want this job. Just a few more years till he can go fishing all day and forget about this. 


## Baker Rivalry
* Name: Billy
* Gender: m
* Age: 33
* Appearance:
  * haircolor: black
  * special: looks mean
* Traits:
  * friendly
  * shy
  * strong
* Special Traits:
  * Intimidation (by looks)
* Description:  
Since young age Billy wanted to be a baker. He was bullied at school for being so tall and that's why he kept to himself. He never got over his shyness. At first sight Billy looks unfriendly and mean but if you started talking with him you'd realise right away that he's just an shy, anxious man. He lives alone with his three turtles.


## Baker Rivalry
* Name: Anna
* Gender: w
* Age: 30
* Appearance:
  * haircolor: black with green streaks
  * special: lots of tattoos
* Traits:
  * sporty
  * humorous
  * sarcastic
* Special Tratis
  * Story Telling
* Description:  
Anna has been working at her current employer for only two years. She loves to bake but doesn't like how her employer treats his employees. Her co-baker Billy and she became good friends. She loves do to anything sports related and starts and ends every day with a workout. She's into arts and tattoos fascinate her. She love that she's able to tell people a story with her tattoos. She rides a motorcycle. She's considering changing her job and become a tattooist. 


## Apprentice Rivalry
* Name: Alfred
* Gender: m
* Age: 16
* Appearance:
  * haircolor: brown
  * special: slender
* Traits:
  * sly
  * extroverted
  * likes to sing
* Special Traits:
  * none
* Description:  
Alfred did well in school and was considered as normal. He likes to play football and sings in a local choir. He didn't know what he wanted to do as a job. When his uncle suggested to become a baker he thought that would be a good idea. He isn't so sure anymore since the only thing he is allowed to do is do the dishes and swap the floors. 