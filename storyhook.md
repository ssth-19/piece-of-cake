# Übersicht über den Story Ablauf in Kurzform

  * Die Charaktere der Bäckerei Piggy erfahren, dass das geheime Kuchenrezept vom Piggybeck von ihrem grössten Konkurrenten, der Bäckerei Honey Pot gestohlen wurde. Sie müssen es zurückholen, bevor es kopiert wird.
  * TODO...


# Piece of cake

Montag Morgen in der  Piggy-Backstube. Martin Piggy, der Patron des Familienbetriebs läuft betrübt durch die Gänge. "Was sollen wir nur tun?", murmelt er traurig vor sich hin.
Als ihr ihn darauf ansprecht, schaut er euch traurig an. Es ist alles verloren. Ich denke, ihr müsst euch einen anderen Job suchen. 
Das Geheimrezept für unseren berühmten Kuchen wurde gestohlen! Und es gibt eigentlich nur eine Möglichkeit wer so was tun würde. Die Bäckerei Honeypot!
Sie versuchen schon seit langem, unsere geheime Zutat zu identifizieren und den Kuchen zu kopieren. Wenn sie das schaffen, sind wir aufgeschmissen. 
Das Rezept ist zwar in einem gesicherten Koffer mit einem Geheimcode, aber es ist nur eine Frage der Zeit, bis sie diesen Code knacken.

Ich weiss nicht mehr was tun - es tut mir leid! Ich hätte den Koffer besser schützen sollen!
Wütend und traurig verlässt er den Raum.

Na, dass könnt ihr natürlich nicht auf euch sitzen lassen. Ihr fasst einen Beschluss: Das Rezept werded ihr euch zurückholen.
Aber es bleibt nicht viel Zeit!

**Jetzt sind die Spieler an der Reihe**:

Was möchtet ihr tun? Der Koffer ist sicher irgendwo in der Bäckerei Honeypot versteckt! Wie wollt ihr vorgehen?

Die Spieler können nun entweder direkt zum Zielgebäude fahren, oder sie könnten z.b. versuchen, sich vorgängig mit Online-Recherchen einen Vorteil zu verschaffen.

